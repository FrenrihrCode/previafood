import React from 'react';
import { StyleSheet, Text, View, ImageBackground, ScrollView} from 'react-native';
import { TextInput , Button  } from 'react-native-paper';
import firebase from './firebase';
import Loader from './loader';

function Registrarse(props) {
    const [nombre, setNombre] = React.useState('');
    const [password, setPassword] = React.useState('');
    const [email, setEmail] = React.useState('');
    const [errEmail, setErrEmail] = React.useState('');
    const [loading, setLoading] = React.useState(false);
    const { navigation } = props;

    async function onRegister(){
      setLoading(true);
      await firebase.auth().createUserWithEmailAndPassword(email, password).then(() => {
        console.log("Usuario: "+email+" se ha registrado");
        //Añadir usuario
        let userId = firebase.auth().currentUser.uid;
        console.log("Usuario: "+userId+" se ha logeado");
        setErrEmail('');
        setLoading(false);
        
        firebase.database().ref('/usuarios/' + userId).set({
          username: nombre,
          saldo: "10.00"
        });
        
        navigation.reset({
          index: 0,
          routes: [{ name: 'Home' }],
        });
      })
      .catch(error => {
        setLoading(false);
        if (error.code === 'auth/email-already-in-use') {
          console.log('El email ya está en uso');
          setErrEmail('El email ya está en uso.');
        }
        if (error.code === 'auth/invalid-email') {
          console.log('La dirección de correo no es válida');
          setErrEmail('La dirección de correo no es válida.');
        }
        if (error.code === 'auth/weak-password') {
          console.log('La contraseña debería tener al menos 6 caracteres');
          setErrEmail('La contraseña debería tener al menos 6 caracteres.');
        } else console.log(error)
      });
    } 

    return ( 
          <View style={styles.container}>
            <ImageBackground source={require('../assets/img/fastFoodBG.jpg')} resizeMode='cover' style={ styles.imgBackground } >
            <Loader loading={loading} />
            <View style={styles.login}>
              <ScrollView>
                <Text style={styles.logo}>Cree su cuenta!</Text>
                <TextInput
                  label='Usuario'
                  value={nombre}
                  mode='outlined'
                  style={styles.inputText}
                  theme={{ colors: { primary: '#800000',underlineColor:'transparent',}}}
                  onChangeText={setNombre}
                />
                <TextInput
                  label='Email'
                  value={email}
                  mode='outlined'
                  keyboardType='email-address'
                  style={styles.inputText}
                  theme={{ colors: { primary: '#800000',underlineColor:'transparent',}}}
                  onChangeText={setEmail}
                />
                <TextInput
                  label='Contraseña'
                  value={password}
                  mode='outlined'
                  secureTextEntry={true}
                  style={styles.inputText}
                  theme={{ colors: { primary: '#800000',underlineColor:'transparent',}}}
                  onChangeText={setPassword}
                />
                
                <Text style={{alignSelf:'center', fontSize: 12, color: '#800000'}}>{errEmail}</Text>
                <Button
                  mode="contained"
                  dark={true}
                  style={styles.btnLogin}
                  color='#800000'
                  onPress={onRegister}
                >REGISTRARSE</Button>
                <View style={{ flexDirection: 'row', alignItems: 'center', justifyContent: 'center', marginVertical: 7}}>
                  <Text>¿Ya tiene un cuenta?</Text>
                  <Button
                    mode="text"
                    style={styles.btnReg}
                    labelStyle={styles.txtReg}
                    onPress={() => navigation.goBack()}
                  >Ingrese aquí</Button>
                </View>
                
              </ScrollView>
              </View>
            </ImageBackground>
          </View>
    )}

const styles = StyleSheet.create({
  
  container: {
    flex: 1
  },
  imgBackground: {
    flex: 1,
    resizeMode: "cover",
    justifyContent: "flex-end"
  },
  login:{
    backgroundColor: '#ffe4b5',
    shadowColor: '#00000021',
    shadowOffset: {
      width: 0,
      height: 6,
    },
    shadowOpacity: 0.37,
    shadowRadius: 7.49,
    elevation: 15,
    borderRadius: 15,
    marginHorizontal: 10,
    paddingLeft: 10,
    paddingRight: 10
  },
  logo:{
    fontWeight:"bold",
    fontSize:40,
    color:"#800000",
    marginBottom:10,
  },
  inputText: {
    backgroundColor: "#ffe4b5",
    marginHorizontal: 10,
  },
  btnLogin: {
    marginHorizontal: 30,
    marginTop: 8
  },
  txtReg: {
    color: '#800000'
  }
});

export default Registrarse