import React, {Component} from 'react';
import {Text, View, StyleSheet,ScrollView} from 'react-native';
import { TextInput, Button, RadioButton, Caption, Title, Paragraph, Switch ,Card} from 'react-native-paper';


export default class TransferenciaTres extends Component{
    constructor(props){
        super(props)
        this.state={
        };
    }

    render(){
        return(
            <ScrollView style={{backgroundColor:"#F2C53D"}}>
            <View style={{ margin:18, justifyContent: 'center'}}>
 
                <Card>
                    <Card.Title title="Compra realizada con exito" subtitle=""/>
                    <Card.Content>
                        <Paragraph style={{fontStyle:'italic', fontFamily:'sans-serif-medium'}}>Su compra ha sido aceptada, para visualizar mayor detalle de su orden puede ver su historial de adquisiciones.</Paragraph>

                    </Card.Content>
                    <Card.Cover style={{marginTop:10}} source={{ uri: 'https://sanjuanboscosalamanca.salesianas.org/wp-content/uploads/2020/05/Gracias.jpg' }} />

                </Card>

                <View style={{ flexDirection:"row", alignSelf:'center'}}>
                    
                    <Button style={{marginTop:10,marginHorizontal:20, borderWidth:1 }} icon="home" color='#800000' mode="outlined" onPress={() => {this.props.navigation.navigate('Restaurantes')} }> 
                        Volver al inicio
                    </Button>


                </View>

            </View>
            </ScrollView>
        )
    }


}

