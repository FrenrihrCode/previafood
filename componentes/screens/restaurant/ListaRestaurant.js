import React, {Component} from 'react';
import {View, FlatList, StyleSheet, Text, Image, TouchableOpacity } from 'react-native';
import { Searchbar, ActivityIndicator } from 'react-native-paper';
import firebase from '../firebase';

function Item({ Titulo,informacion,longitud,latitud,imagen,navigation,platos,direccion }) {
  return (
      <View style= {styles.container}>
      <View style={{flex:1, flexDirection: 'row'}}>
          <Image style={styles.imageView} source = {{uri: imagen}}/>
          <View style= {styles.textView}>
              <Text style= {styles.title}>{Titulo}</Text>
          </View>
          <View style={styles.textvolver}>
            <TouchableOpacity 
              onPress={() => {
                    /* 1. Navigate to the Details route with params */
                    navigation.navigate('Informacion', {
                        Titulo: Titulo, informacion: informacion,
                        longitud: longitud, latitud:latitud,
                        imagen: imagen, direccion: direccion,
                        platos: platos,
                    });
                    }}>
              <Image source={require('./boton1.png') } style={{height: 40, width: 40}}/>
            </TouchableOpacity >
          </View>
      </View>
      </View>
  );
}
  

export default class ListaRestaurant extends Component{
    
    constructor(props){
        super(props);
        this.state = {
          text: "",
          items: [],
          data: [],
      };        
    }

    async componentDidMount() {
      let data = []
      await firebase.database().ref('/restaurant')
      .once('value', function(snapshot) {
        snapshot.forEach(function(childSnapshot) {
          var childData = childSnapshot.val();
          data.push(childData)
        });
      });
      this.setState({ data: data });
      this.setState({ items: data });
  }

    renderSeparatorView = () => {
      return (
        <View style={{
            height: 1, 
            width: "100%",
            backgroundColor: "#a9a9a9",
          }}
        />
      );
    };

    searchFilterFunction = query  => {
      const textData = query.toUpperCase();
      const newData = this.state.items.filter(item => item.titulo.toUpperCase().indexOf(textData) > -1 );
      this.setState({ data: newData });
      this.setState({text: query})  
    };
        
    renderHeader = () => {    
      return (      
        <Searchbar        
          placeholder="Buscar restaurant"
          onChangeText={this.searchFilterFunction}
          value= {this.state.text}         
        />    
      );  
    };

    render(){
        
        return(
            <View style={styles.list}>
            <FlatList style={styles.container}
                data={this.state.data}
                renderItem={({ item }) => <Item platos={item.platos}
                  Titulo={item.titulo} informacion={item.informacion}
                  latitud={item.latitud} longitud={item.longitud} direccion={item.direccion}
                  navigation={this.props.navigation} imagen={item.imagen}/>}
                keyExtractor={item => item.id}
                ListEmptyComponent={() => (<ActivityIndicator style={{marginTop: 25}} animating={true} color='#800000' />)}
                ListHeaderComponent={this.renderHeader}
                ItemSeparatorComponent={this.renderSeparatorView}
            />
            </View>
        )
    }
}

const styles = StyleSheet.create({
    loading: {
        flex: 1,
        padding:50,
        justifyContent: 'center',
        alignContent: 'center',
      },
    list: {
      flex: 1,
      backgroundColor: '#F2C53D',
    },
    container: {
      marginTop: 15,
      margin: 20,
      backgroundColor: '#ffe4b5'
    },
    imageView: {
        width: '40%',
        height: 80 ,
        margin: 5,
        borderRadius: 30, 
    },
    textView: {
        width:'40%', 
        textAlignVertical:'center',
        textAlign:'justify',
        justifyContent: 'center',
        fontFamily: 'Pacifico-Regular',
        margin: 10,
    },
    textvolver:{
      marginRight: 20,
      justifyContent: 'center'
    },
    item: {
      backgroundColor: 'gray',
      padding: 10
    },
    title: {
      fontSize: 20,
      color: '#000000',
      fontWeight: 'bold',
      fontFamily: 'Bangers',
    },
    titleloading: {
        fontSize: 40,
        color: '#c43c00',
        fontWeight: 'bold',
        fontFamily: 'Bangers',
      },
  });